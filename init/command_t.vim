" Steve Ellis & Suman Gurung
" Small default height for CommandT
let g:CommandTMaxHeight=20
let g:CommandTMaxFiles=20000

set wildignore+=**/awx/lib/site-packages/**
set wildignore+=**/awx/ui/**
set wildignore+=**/node_modules/**


let g:CommandTNeverShowDotFiles=1
let g:CommandTScanDotDirectories=0
